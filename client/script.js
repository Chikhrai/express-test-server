$(document).ready(() => {
  $.ajax({
    type: "GET",
    url: "/list"
  })
    .then(data => {
      data.forEach( e => {
        const el = document.createElement("li");
        el.textContent = e.name;
        $("#content > ul")[0].append(el);
      });
    })
    .catch(console.error);
});

$("#addTask").on("submit", e => {
  e.preventDefault();
  $.ajax({
    type: "POST",
    url: "/list",
    data: {
      name: $("#taskName").val()
    }
  })
    .then(() => {
      location.reload();
    })
    .catch(console.error);
});
