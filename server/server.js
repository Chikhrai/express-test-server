const PORT = process.env.PORT || 3000;
const express = require("express");
const path = require("path");
const app = express();

console.log("ENV", process.env.NODE_ENV);
if (process.env.NODE_ENV !== "prod") {
  const morgan = require("morgan");
  app.use(morgan("combined"));
}

app.use(express.static(path.join(__dirname, "..", "client")));
app.use(express.json());
app.use(express.urlencoded());

const tasks = [];
let inc = 0;
app.get("/list", (req, res) => {
  res.send(tasks);
});

app.post("/list", (req, res) => {
  tasks.push({name: req.body.name, id: inc++});
  res.send("ok");
});

app.put("/list/:variable", (req, res) => {
  req.params.variable = parseInt(req.params.variable);
  console.log(tasks, req.params.variable);
  const taskId = tasks.findIndex( e=> e.id === req.params.variable);
  if(taskId >= 0) tasks[taskId].checked = req.body.checked;
  res.send("ok");
});

app.listen(PORT, () => {
  console.log(`Server stared on port: ${PORT}`);
});
